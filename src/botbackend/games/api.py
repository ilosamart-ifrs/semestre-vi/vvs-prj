from ninja import Router, Schema

from .jankenpon import Opcoes, Resultado


class PlayerIn(Schema):
    jogador1: Opcoes
    jogador2: Opcoes


class PlayerOut(Schema):
    jogador1: Resultado
    jogador2: Resultado


router = Router()

@router.post('/jankenpon', response=PlayerOut)
def jankenpon(request, jogada: PlayerIn):
    resultado_jogador1 = Opcoes.compara(jogada.jogador1, jogada.jogador2)
    resultado_jogador2 = Resultado.VENCE
    if resultado_jogador1 == Resultado.EMPATA:
        resultado_jogador2 = resultado_jogador1
    elif resultado_jogador1 == Resultado.VENCE:
        resultado_jogador2 = Resultado.PERDE
    retval = PlayerOut(jogador1=resultado_jogador1, jogador2=resultado_jogador2,)
    return retval