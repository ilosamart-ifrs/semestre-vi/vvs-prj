from __future__ import annotations
from enum import Enum, IntEnum


class Resultado(str, Enum):
        VENCE = 1
        EMPATA = 0
        PERDE = -1


class Opcoes(str, Enum):
    PEDRA = 'Pedra'
    PAPEL = 'Papel'
    TESOURA = 'Tesoura'

    def compara(valor1: Opcoes, valor2: Opcoes, ) -> Resultado:
        """Verifica se valor1 vence de valor2."""
        retval = Resultado.PERDE
        if valor1 == valor2:
            retval = Resultado.EMPATA
        if (valor1 == Opcoes.PEDRA and valor2 == Opcoes.TESOURA) or \
            (valor1 == Opcoes.TESOURA and valor2 == Opcoes.PAPEL) or \
            (valor1 == Opcoes.PAPEL and valor2 == Opcoes.PEDRA):
            retval = retval = Resultado.VENCE
        return retval
