from ninja import NinjaAPI

from games.api import router as games_router


api = NinjaAPI(title='Backend API', description='API bonita', version='0.0.1')
api.add_router("/games/", games_router)


@api.get("/hello")
def hello(request):
    return "Hello world"
