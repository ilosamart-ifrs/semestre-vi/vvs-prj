from pathlib import Path
import time

import testcontainers.compose
import requests


root_path = Path(__file__).absolute().parent.parent.parent
compose_integration = 'docker-compose-integration.yml'


def test_main():
    with testcontainers.compose.DockerCompose(root_path, compose_file_name=compose_integration) as compose:
        host = compose.get_service_host("app", 8000)
        port = compose.get_service_port("app", 8000)
        app_url = f'http://localhost:{port}'

        response = requests.get(f'{app_url}/api/hello')
        assert 'Hello world' == response.json(), "TEXTO QUE EU ESPERAVA NÃO FOI ENCONTRADO"

        empate = {
            "jogador1": "Pedra",
            "jogador2": "Pedra",
        }

        JOGADAS = {
            'EMPATE': {
                'jogada': {
                    "jogador1": "Pedra",
                    "jogador2": "Pedra",
                },
                'expectativa': {
                    "jogador1": "0",
                    "jogador2": "0"
                },
                'texto_erro': 'NÃO DEU EMPATE!',
            },
            'VITÓRIA JOGADOR 1': {
                'jogada': {
                    "jogador1": "Papel",
                    "jogador2": "Pedra",
                },
                'expectativa': {
                    "jogador1": "1",
                    "jogador2": "-1"
                },
                'texto_erro': "JOGADOR 1 NÃO VENCEU!",
            },
            'VITÓRIA JOGADOR 2': {
                'jogada': {
                    "jogador1": "Tesoura",
                    "jogador2": "Pedra",
                },
                'expectativa': {
                    "jogador1": "-1",
                    "jogador2": "1"
                },
                'texto_erro': "JOGADOR 2 NÃO VENCEU!",
            },
        }

        for descricao, parametros in JOGADAS.items():
            print(f'Teste: {descricao}')
            response = requests.post(f'{app_url}/api/games/jankenpon', json=parametros['jogada'])
            assert parametros['expectativa'] == response.json(), parametros['texto_erro']


if __name__ == '__main__':
    test_main()
